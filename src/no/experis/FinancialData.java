package no.experis;

import org.json.*;

import java.util.ArrayList;

public class FinancialData {

    private ArrayList<ExchangeRate> exchangeRates = new ArrayList<>();
    private boolean first = true;

    public void checkChange() throws Exception{
        JSONObject jo = new RequestData().getData();
        JSONArray arr = new JSONArray(jo.get("forexList").toString());

        // even if its duplicate code, i feel its better due to limiting the amount of if-s to check
        if(first){
            first = false;
            for(int i = 0; i < arr.length(); i++){
                String[] split = arr.get(i).toString().split("\":\"|\",\"|\":|\"|,");
                appendList(split);
            }
        }else {
            for(int i = 0; i < arr.length(); i++){
                String[] split = arr.get(i).toString().split("\":\"|\",\"|\":|\"|,");
                for(String s : split){
                    changeElemInList(split);
                }
            }
        }
    }

    private void appendList(String[] data){
        StringBuilder currency = new StringBuilder();
        double bid = 0, ask = 0, changes = 0; // all will be set
        for(int i = 0; i < data.length; i++){
            if(data[i].equalsIgnoreCase("ticker")){
                currency.append(data[++i]);
            }else if(data[i].equalsIgnoreCase("bid")){
                bid = Double.parseDouble(data[++i]);
            }else if(data[i].equalsIgnoreCase("ask")){
                ask = Double.parseDouble((data[++i]));
            }else if(data[i].equalsIgnoreCase("changes")){
                changes = Double.parseDouble(data[++i]);
            }
        }
        this.exchangeRates.add(new ExchangeRate(currency.toString(), bid, ask, changes));
    }

    private boolean changeElemInList(String[] data){
        StringBuilder currency = new StringBuilder();
        double bid = 0, ask = 0, changes = 0; // all will be set
        for(int i = 0; i < data.length; i++){
            if(data[i].equalsIgnoreCase("ticker")){
                currency.append(data[++i]);
            }else if(data[i].equalsIgnoreCase("bid")){
                bid = Double.parseDouble(data[++i]);
            }else if(data[i].equalsIgnoreCase("ask")){
                ask = Double.parseDouble((data[++i]));
            }else if(data[i].equalsIgnoreCase("changes")){
                changes = Double.parseDouble(data[++i]);
            }
        }
        for(ExchangeRate e : this.exchangeRates){
            if(e.getCurrency().equalsIgnoreCase(currency.toString())){
                e.setAsk(ask);
                e.setBid(bid);
                e.setChanges(changes);
                return true;
            }

        }

        return false;
    }

    //getter

    public ArrayList<ExchangeRate> getExchangeRates() {
        return this.exchangeRates;
    }
}
