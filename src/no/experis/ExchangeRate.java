package no.experis;

public class ExchangeRate {
    private String currency;
    private double bid;
    private double ask;
    private double changes;

    public ExchangeRate(String currency, double bid, double ask, double changes){
        this.currency = currency;
        this.bid = bid;
        this.ask = ask;
        this.changes = changes;
    }

    //getters
    public String getCurrency() {
        return currency;
    }

    public double getBid() {
        return bid;
    }

    public double getAsk() {
        return ask;
    }

    public double getChanges() {
        return changes;
    }

    //setters
    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public void setBid(double bid) {
        this.bid = bid;
    }

    public void setAsk(double ask) {
        this.ask = ask;
    }

    public void setChanges(double changes) {
        this.changes = changes;
    }
}
