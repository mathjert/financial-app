package no.experis;

import java.util.ArrayList;

public class Main {

    public static void main(String[] args) {
        FinancialData data = new FinancialData();
        String redBg = "\u001B[101m";
        String greenBg = "\u001B[102m";
        String blackFg = "\u001B[90m";
        String reset = "\u001B[0m";
        System.out.println(reset);
        try{
            while(true){
                //IntelliJ IDEA console is not a real terminal, so there is no command to clear it from your Java code.
                //https://intellij-support.jetbrains.com/hc/en-us/community/posts/115000585870-Clear-IntelliJ-console-from-java
                //new ProcessBuilder("cmd", "/c", "cls").inheritIO().start().waitFor();
                System.out.println(reset);
                System.out.println("Made by Mathias Christian Hjertholm and Alexander Brorson Olsen");
                //System.out.println("\033[H\033[2J");
                //System.out.flush();
                data.checkChange();
                ArrayList<ExchangeRate> tmp = data.getExchangeRates();
                StringBuilder sb = new StringBuilder();
                sb.append("Currency").append("          ").append("         ask         ").append("         bid         ").append("       changes       ");

                System.out.println(sb.toString());
                for(ExchangeRate e : tmp) {
                    System.out.print(reset);

                    int res = 18 - e.getCurrency().length();
                    StringBuilder currency = new StringBuilder();
                    currency.append(e.getCurrency()).append(makeSpaces(res));

                    StringBuilder rate = new StringBuilder();
                    res = 21 - Double.toString(e.getAsk()).length();
                    int modRes = res % 2;
                    rate.append(makeSpaces(res/2)).append(e.getAsk()).append(makeSpaces((res/2)+modRes));
                    res = 21 - Double.toString(e.getBid()).length();
                    modRes = res % 2;
                    rate.append(makeSpaces(res/2)).append(e.getBid()).append(makeSpaces((res/2)+modRes));
                    res = 21 - Double.toString(e.getChanges()).length();
                    modRes = res % 2;
                    rate.append(makeSpaces(res/2)).append(e.getChanges()).append(makeSpaces((res/2)+modRes));

                    System.out.print(currency.toString());
                    if (e.getChanges() < 0) {
                        System.out.println(blackFg + redBg + rate.toString());
                    } else {
                        System.out.println(blackFg + greenBg + rate.toString());
                    }
                }
                Thread.sleep(5000);
            }



        }catch (Exception e){
            System.out.println("Caught Exception");
            System.out.println(e.getMessage());
        }
    }

    private static String makeSpaces(int spaces){
        StringBuilder sb = new StringBuilder();
        for(int i = 0; i < spaces; i++)
            sb.append(" ");
        return sb.toString();
    }
}
