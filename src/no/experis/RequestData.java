package no.experis;
import org.json.*;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.net.URL;
import java.net.URLConnection;

public class RequestData {
    private final String URL = "https://financialmodelingprep.com/api/v3/forex/";

    // precode
    private String requestURL(String url) throws Exception{
        // Set URL
        URLConnection connection = new URL(url).openConnection();
        // Set property - avoid 403 (CORS)
        connection.setRequestProperty("User-Agent", "Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.11 (KHTML, like Gecko) Chrome/23.0.1271.95 Safari/537.11");
        // Creat connection
        connection.connect();
        // Create a buffer of the input
        BufferedReader buffer  = new BufferedReader(new InputStreamReader(connection.getInputStream()));
        // Convert the response into a single string
        StringBuilder stringBuilder = new StringBuilder();
        String line;
        while ((line = buffer.readLine()) != null) { // buffer.ready() works here too?
            stringBuilder.append(line);
        }
        return stringBuilder.toString();
    }
    public JSONObject getData() throws Exception{
        return new JSONObject(requestURL(this.URL));
        //return new JSONObject("{\"forexList\":[{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/USD\",\"high\":\"1.10791\",\"low\":\"1.10365\",\"ask\":\"1.10486\",\"changes\":-0.2086419307055922,\"bid\":\"1.10484\",\"open\":\"1.10716\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"USD/JPY\",\"high\":\"108.275\",\"low\":\"108.016\",\"ask\":\"108.208\",\"changes\":0.07213672684225061,\"bid\":\"108.204\",\"open\":\"108.128\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"GBP/USD\",\"high\":\"1.25148\",\"low\":\"1.24384\",\"ask\":\"1.24640\",\"changes\":-0.2624734925779178,\"bid\":\"1.24634\",\"open\":\"1.24965\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/GBP\",\"high\":\"0.88815\",\"low\":\"0.88490\",\"ask\":\"0.88650\",\"changes\":0.08128704487721625,\"bid\":\"0.88644\",\"open\":\"0.88575\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"USD/CHF\",\"high\":\"0.99583\",\"low\":\"0.99226\",\"ask\":\"0.99561\",\"changes\":0.2214683350614666,\"bid\":\"0.99553\",\"open\":\"0.99337\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/JPY\",\"high\":\"119.827\",\"low\":\"119.411\",\"ask\":\"119.550\",\"changes\":-0.1369943113947418,\"bid\":\"119.548\",\"open\":\"119.713\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/CHF\",\"high\":\"1.10062\",\"low\":\"1.09844\",\"ask\":\"1.09997\",\"changes\":0.016822005001125684,\"bid\":\"1.09990\",\"open\":\"1.09975\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"USD/CAD\",\"high\":\"1.32720\",\"low\":\"1.32390\",\"ask\":\"1.32636\",\"changes\":0.1272034122220916,\"bid\":\"1.32631\",\"open\":\"1.32465\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"AUD/USD\",\"high\":\"0.68731\",\"low\":\"0.68284\",\"ask\":\"0.68329\",\"changes\":-0.48499854354790917,\"bid\":\"0.68325\",\"open\":\"0.68660\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"GBP/JPY\",\"high\":\"135.289\",\"low\":\"134.592\",\"ask\":\"134.871\",\"changes\":-0.1920444033302518,\"bid\":\"134.860\",\"open\":\"135.125\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"AUD/CAD\",\"high\":\"0.91152\",\"low\":\"0.90588\",\"ask\":\"0.90630\",\"changes\":-0.36392822587738977,\"bid\":\"0.90612\",\"open\":\"0.90952\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"AUD/CHF\",\"high\":\"0.68382\",\"low\":\"0.67917\",\"ask\":\"0.68028\",\"changes\":-0.2668386945430085,\"bid\":\"0.68020\",\"open\":\"0.68206\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"AUD/JPY\",\"high\":\"74.294\",\"low\":\"73.885\",\"ask\":\"73.937\",\"changes\":-0.41285021551722195,\"bid\":\"73.930\",\"open\":\"74.240\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"AUD/NZD\",\"high\":\"1.08198\",\"low\":\"1.07799\",\"ask\":\"1.07879\",\"changes\":-0.11666774692358622,\"bid\":\"1.07867\",\"open\":\"1.07999\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"CAD/CHF\",\"high\":\"0.75098\",\"low\":\"0.74895\",\"ask\":\"0.75071\",\"changes\":0.08733449779329873,\"bid\":\"0.75058\",\"open\":\"0.74999\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"CAD/JPY\",\"high\":\"81.702\",\"low\":\"81.536\",\"ask\":\"81.591\",\"changes\":-0.04900279315921774,\"bid\":\"81.585\",\"open\":\"81.628\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"CHF/JPY\",\"high\":\"108.999\",\"low\":\"108.671\",\"ask\":\"108.692\",\"changes\":-0.14836931557187832,\"bid\":\"108.685\",\"open\":\"108.850\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/AUD\",\"high\":\"1.61839\",\"low\":\"1.61162\",\"ask\":\"1.61706\",\"changes\":0.2747184600883027,\"bid\":\"1.61692\",\"open\":\"1.61256\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/CAD\",\"high\":\"1.46786\",\"low\":\"1.46389\",\"ask\":\"1.46542\",\"changes\":-0.0814843100085804,\"bid\":\"1.46527\",\"open\":\"1.46654\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/NOK\",\"high\":\"9.90871\",\"low\":\"9.86469\",\"ask\":\"9.89898\",\"changes\":0.0718300019916195,\"bid\":\"9.89805\",\"open\":\"9.89141\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"EUR/NZD\",\"high\":\"1.74745\",\"low\":\"1.74011\",\"ask\":\"1.74440\",\"changes\":0.15962975085126324,\"bid\":\"1.74422\",\"open\":\"1.74153\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"GBP/CAD\",\"high\":\"1.66051\",\"low\":\"1.65003\",\"ask\":\"1.65315\",\"changes\":-0.1320058239640341,\"bid\":\"1.65294\",\"open\":\"1.65523\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"GBP/CHF\",\"high\":\"1.24307\",\"low\":\"1.23757\",\"ask\":\"1.24091\",\"changes\":-0.03464166022170102,\"bid\":\"1.24079\",\"open\":\"1.24128\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"NZD/JPY\",\"high\":\"68.925\",\"low\":\"68.477\",\"ask\":\"68.541\",\"changes\":-0.29168303292163866,\"bid\":\"68.536\",\"open\":\"68.739\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"NZD/USD\",\"high\":\"0.63635\",\"low\":\"0.63287\",\"ask\":\"0.63345\",\"changes\":-0.36728273692488805,\"bid\":\"0.63338\",\"open\":\"0.63575\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"USD/NOK\",\"high\":\"8.97260\",\"low\":\"8.91323\",\"ask\":\"8.95984\",\"changes\":0.28257124724228555,\"bid\":\"8.95871\",\"open\":\"8.93403\"},{\"date\":\"2019-09-18 06:28:58\",\"ticker\":\"USD/SEK\",\"high\":\"9.72861\",\"low\":\"9.65910\",\"ask\":\"9.71571\",\"changes\":0.43426883003293465,\"bid\":\"9.71346\",\"open\":\"9.67258\"}]}");
    }
}
